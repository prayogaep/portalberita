@extends('layouts.master')

@push('scripts')
<script src="https://cdn.tiny.cloud/1/g4mhcbddpbwzgbbof0zgm4t1jtf4nrw1acffbt5f0hkdsqnn/tinymce/5/tinymce.min.js" referrerpolicy="origin"></script>
<script>
    tinymce.init({
      selector: 'textarea',
      plugins: 'a11ychecker advcode casechange export formatpainter linkchecker autolink lists checklist media mediaembed pageembed permanentpen powerpaste table advtable tinycomments tinymcespellchecker',
      toolbar: 'a11ycheck addcomment showcomments casechange checklist code export formatpainter pageembed permanentpen table',
      toolbar_mode: 'floating',
      tinycomments_mode: 'embedded',
      tinycomments_author: 'Author name',
   });
  </script>
@endpush
@section('content')
<div class="container">
    <div class="ml-4 mr-4">
    <h2>Tambah Berita</h2>
    <form action="/berita" method="POST">
        @csrf
        <div class="form-group">
            <label for="judul">Judul Berita</label>
            <input type="text" class="form-control" name="judul" id="judul" placeholder="Masukkan Judul">
            @error('judul')
            <div class="alert alert-danger">
                {{ $message }}
            </div>
            @enderror
        </div>
        <div class="form-group">
            <label for="isi">Isi Berita</label>
            <textarea name="isi" id="isi" class="form-control" cols="30" rows="10"></textarea>
            @error('isi')
            <div class="alert alert-danger">
                {{ $message }}
            </div>
            @enderror
        </div>
        <div class="form-group">
            <label for="kategori_id">Kategori Berita</label>
            <select class="form-control" name="kategori_id" id="kategori_id">
                <option value="" selected>-- Pilih Kategori --</option>
                @foreach ($kategori as $item)
                    <option value="{{$item->id}}">{{$item->nama}}</option>    
                @endforeach
            </select>
            @error('kategori_id')
                <div class="alert alert-danger">
                    {{ $message }}
                </div>
            @enderror
        </div>
        <button type="submit" class="btn btn-primary">Tambah</button>
    </form>
    </div>
</div>
@endsection