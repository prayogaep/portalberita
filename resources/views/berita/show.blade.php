@extends('layouts.master')
@section('content')
<div class="container my-2">
    <h2>{{$berita->judul}}</h2>
    <h4>Kategori : {{$berita->kategori->nama}}</h4>
    <h4>Penulis : {{$berita->user->name}}</h4>
    <p>{{$berita->isi}}</p>
    <hr>
    @forelse ($berita->komentar as $item)
        <div class="container">
            <div class="row">
                <div class="col-md-4">
                    {{$item->user->name}}
                </div>
                <div class="col-md-8">
                    {{$item->isi}}
                </div>
            </div>
        </div>
    @empty
        Tidak Ada Komentar
    @endforelse
    <form action="/komentar" method="post" class="my-3">
        @csrf
        <input type="hidden" value="{{$berita->id}}" name="berita_id">
        <textarea name="isi" class="form-control" id="isi" cols="30" rows="10"></textarea>
        <button type="submit" class="btn btn-primary my-2">Kirim Komentar</button>
    </form>
</div>

@endsection