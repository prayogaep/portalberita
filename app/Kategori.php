<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Kategori extends Model
{
    protected $table = "kategori";
    protected $fillable = ["nama"];
    public $timestamp = false;

    public function berita()
    {
        return $this->hasMany('App\Berita');
    }
}
